#include <iostream>
class Animal
{
public:
    virtual void makeSound() const = 0;
};

class Cat : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Meow\n";
    }
};

class Dog : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Woof\n";
    }
};

class Cow : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Mooo\n";
    }
};

class Sheep : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Beee\n";
    }
};

class Mouse : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Squeak\n";
    }
};

int main()
{
    Animal* animals[5];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Cow();
    animals[3] = new Sheep();
    animals[4] = new Mouse();

    for (Animal* a : animals)
        a->makeSound();
}